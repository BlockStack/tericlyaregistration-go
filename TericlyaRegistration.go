package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/bwmarrin/discordgo"
)

const (
	commandStr       = "!"
	sayHello         = "Hello! I'm the Tericlya Registration bot.  Please answer the follwoing questions."
	ageQ             = "Are you over 16 years old? (yes/no)"
	usernameQ        = "What is your Minecraft user name?"
	errorResp        = "Uh oh! Something has gone wrong please you !register in a public channel to restart your registration."
	errorYesNo       = "Please answer yes or no."
	completedQ       = "Thank you for registering.  Tericlya is in Alpha.  Your registration will be handled by an Admin."
	completeRegError = "Your registration is all ready complete. If you want to register again use !register."

	rulesQ = `	
1. Be respectful.
2. Do not steal. You may borrow from farms, but you must replant what you take.
3. Do not litter. Leaving your items in the open, with little indication of ownership, is both rude, and can cause confusion for scavengers. Please clean up your mess.
4. Do not grief. Accidents happen, but please fix your mistakes. 
5. Do not spawn withers outside of the Wither world.
6. You may not slaughter animals, which are tamed, or caged.
7. Do not scam. Tricking players, trapping players to kill them, lieing for personal gain, and similar nefarious actions, will result in a permaban.
8. Do not spam. Repeatedly typing the same thing, repeated attempts to make people go to another game, or harassment about other games, will result in a ban.
9. Do not hack. No X-ray, no automatic mining/botting.
10. Do not trade with villagers you do not own, unless given permission.
11. Do not build within 1000 blocks of another town/spawn. When you go to claim, it will alert you. This is considered griefing, and repeated offenses will result in a ban.
12. Dark rooms, AFK-farms, and mob-farms, are disabled. Redstone still works. Automated agriculture is fine.

Contact staff, on Discord, with grievances. Errors in banishment are extraordinarily rare. Choose your actions wisely.

**Do you agree to the server rules? (yes/no)**`

	alphaTOS = `
Closed-alpha means:
	- Everyone is not permitted to join without invitation.
	- There will be major bugs.
	- There will be missing, major, features.
	- We will be changing mechanics. 
	- We may need to do emergency restarts. 
	- The server may go down on its own. 
	- There will be extended, unplanned, down-time.
	- Players may lose items, and they may not be recoverable.
	- Players are expected to participate in debugging, using the proper reporting system.
	- Players are required to spend time as a visitor before promotion to play-able rank.
	
	**Do you agree to the terms of service? (yes/no)**
`
)

var (
	activeRegs                         map[string]*answers
	applicationChannelID, discordBotID string
)

type answers struct {
	Over18       string
	AgreeToRules string
	Username     string
	TOS          string
}

func main() {
	applicationChannelID = os.Getenv("DISCORD_APPLICATION_CHANNEL_ID")
	discordBotID = os.Getenv("DISOCRD_BOT_ID")
	activeRegs = make(map[string]*answers)
	discord, err := discordgo.New("Bot " + discordBotID)
	errCheck("error creating discord session", err)
	errCheck("error retrieving account", err)

	discord.AddHandler(commandHandler)
	discord.AddHandler(func(discord *discordgo.Session, ready *discordgo.Ready) {
		err = discord.UpdateStatus(0, "Tericlya Minecraft!")
		if err != nil {
			fmt.Println("Error attempting to set my status")
		}
		servers := discord.State.Guilds
		fmt.Printf("Teryiclya Registration bot has started on %d servers", len(servers))
	})

	err = discord.Open()
	errCheck("Error opening connection to Discord", err)
	defer discord.Close()

	<-make(chan struct{})

}

func errCheck(msg string, err error) {
	if err != nil {
		fmt.Printf("%s: %+v", msg, err)
		panic(err)
	}
}

func commandHandler(session *discordgo.Session, message *discordgo.MessageCreate) {
	user := message.Author
	if user.ID == session.State.User.ID || user.Bot {
		//Do nothing because the bot is talking
		return
	}
	content := message.Content
	reg, exists := activeRegs[user.ID]

	//if !register start a new registration
	if strings.HasPrefix(content, commandStr+"register") {
		//start private convo
		private, err := session.UserChannelCreate(message.Author.ID)
		errCheck("error creating private user session.", err)
		session.ChannelMessageSend(private.ID, sayHello)
		session.ChannelMessageSend(private.ID, ageQ)
		activeRegs[user.ID] = &answers{}
		return
	}

	//handle private conversations
	if channel, _ := session.Channel(message.ChannelID); channel.Type == discordgo.ChannelTypeDM {
		if strings.HasPrefix(content, commandStr+"register") {
			session.ChannelMessageSend(message.ChannelID, sayHello)
			session.ChannelMessageSend(message.ChannelID, ageQ)
			activeRegs[user.ID] = &answers{}
			return
		}
		if !exists {
			//user is private messaging but no registration exists!
			session.ChannelMessageSend(message.ChannelID, errorResp)
			return
		}
		//Okay so likely the user is responding to a question
		if reg.Over18 == "" {
			if strings.EqualFold(content, "yes") {
				reg.Over18 = "yes"
			} else if strings.EqualFold(content, "no") {
				reg.Over18 = "no"
			} else {
				session.ChannelMessageSend(message.ChannelID, errorYesNo)
				return
			}
			session.ChannelMessageSend(message.ChannelID, rulesQ)
			return
		}

		if reg.AgreeToRules == "" {
			if strings.EqualFold(content, "yes") {
				reg.AgreeToRules = "yes"
			} else if strings.EqualFold(content, "no") {
				reg.AgreeToRules = "no"
			} else {
				session.ChannelMessageSend(message.ChannelID, errorYesNo)
				return
			}
			session.ChannelMessageSend(message.ChannelID, usernameQ)
			return
		}

		if reg.Username == "" {
			reg.Username = content
			session.ChannelMessageSend(message.ChannelID, alphaTOS)
			return
		}

		if reg.TOS == "" {
			if strings.EqualFold(content, "yes") {
				reg.TOS = "yes"
			} else if strings.EqualFold(content, "no") {
				reg.TOS = "no"
			} else {
				session.ChannelMessageSend(message.ChannelID, errorYesNo)
				return
			}
			//They have completed all questions so we post to the applications channel
			session.ChannelMessageSend(message.ChannelID, completedQ)
			session.ChannelMessageSend(applicationChannelID, "MCUser: "+reg.Username+", Over 16: "+reg.Over18+", Agreed to Rules: "+reg.AgreeToRules+", Agreed to TOS: "+reg.TOS)
			return
		}

		//Typed something after completion
		session.ChannelMessageSend(message.ChannelID, completeRegError)
		return
	}

	fmt.Printf("Message: %+v || From: %s\n", message.Message, message.Author)
}
