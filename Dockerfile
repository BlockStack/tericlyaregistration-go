FROM golang:1.11.2-stretch
WORKDIR /go/src/TericlyaRegistration/
RUN mkdir -p /go/src/TericlyaRegistration/
COPY . /go/src/TericlyaRegistration/
RUN curl https://glide.sh/get | sh
RUN glide install
#RUN ls -l /go/src/TericlyaRegistration/
RUN CGO_ENABLED=0 GOOS=linux go build -a ../...
#RUN ls -l /go/src/

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /go/src/TericlyaRegistration/TericlyaRegistration /go/bin/TericlyaRegistration
CMD ["/go/bin/TericlyaRegistration"]  